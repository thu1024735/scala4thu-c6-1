package apps

import cc.ListNode
/**
  * Created by mac043 on 2017/4/24.
  */
object ListNodeApp extends App{

  val listNode=ListNode(1,ListNode(2,ListNode(4,null)))
  println(listNode)

  val size=listNode.size
  println(size)
}
